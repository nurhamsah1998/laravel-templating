@extends('layout.master')


@section('title')
    Halaman Form
@endsection
@section('content')
<form action="/welcome" method="POST">
  @csrf
  <h2>Buat Akun Baru</h2>
  <h4>Sign Up Form</h4>
  <div>
    <p>First name :</p>
    <input name="fname" style="border: black 1px solid" />
  </div>
  <div>
    <p>Last name :</p>
    <input name="lname" style="border: black 1px solid" />
  </div>
  <div>
    <p>Gender</p>
    <input  type="radio" id="male" name="gender" value="male2" />
    <label for="male">Male</label><br />
    <input type="radio" id="female" name="gender" value="female2" />
    <label for="female">Female</label><br />
  </div>
  <div>
    <p>Nationality</p>
    <select style="border: black 1px solid">
      <option>Indonesia</option>
      <option>Amerika</option>
      <option>Inggris</option>
    </select>
  </div>
  <div >
    <p>Language Spoken</p>
    <input  type="checkbox" id="Bi" />
    <label for="Bi">Bahasa Indonesia</label><br />
    <input type="checkbox" id="En" />
    <label for="En">English</label><br />
    <input type="checkbox" id="Ot" />
    <label for="Ot">Other</label><br />
  </div>
  <div>
    <p>Bio</p>
    <textarea rows="10" style="border: black 1px solid" cols="30"> </textarea>
  </div>
  <button style="border: black 1px solid" type="submit">Sign Up</button>
</form>
  
@endsection