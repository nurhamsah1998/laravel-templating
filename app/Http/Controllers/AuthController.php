<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register()
    {
        return view('page.register');
    }

    public function Welcome(Request $request)
    {

        $nama_depan = $request->input('fname');
        $nama_belakang = $request->input('lname');

        return view('page.welcome',["nama_depan"=> $nama_depan, "nama_belakang"=>$nama_belakang]);
    }
}
