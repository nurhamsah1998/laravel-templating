<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    use HasFactory;
    ///stackoverflow start
    ///https://stackoverflow.com/a/28277980/18038473
    public $timestamps = false;
    /// stackoverflow end

    
    protected $table = "cast";
    protected $fillable = ["nama","umur","bio"];
}
